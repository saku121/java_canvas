package com.myapp.myapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.HashMap;

public class AppView extends View {
    public static final float TOUCH = 10;
    private Bitmap bitmap;
    private Canvas bitmapCanvas;
    private Paint paintScreen;
    private Paint paintLine;
    private HashMap <Integer, Path> pathMap;
    private HashMap <Integer, Point> prePointMap;

    public AppView(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    public AppView(Context context)
    {
        super(context);
        init();
    }

    void init()
    {
        paintScreen = new Paint();

        paintLine = new Paint();
        paintLine.setAntiAlias(true);
        paintLine.setColor(Color.BLACK);
        paintLine.setStyle(Paint.Style.STROKE);
        paintLine.setStrokeWidth(7);
        paintLine.setStrokeCap(Paint.Cap.ROUND);

        pathMap = new HashMap<>();
        prePointMap = new HashMap<>();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh)
    {
        bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        bitmapCanvas = new Canvas(bitmap);
        bitmap.eraseColor(Color.WHITE);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        canvas.drawBitmap(bitmap, 0, 0, paintScreen);

        for(Integer key:pathMap.keySet())
        {
            canvas.drawPath(pathMap.get(key), paintLine);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int action = event.getActionMasked(); //my event type
        int actionIndex = event.getActionIndex(); // mt finger

        if(action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_POINTER_UP)
        {
            touchStarted(event.getX(actionIndex),
                    event.getY(actionIndex),
                    event.getPointerId(actionIndex));
        }
        else if(action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_POINTER_UP)
        {
            touchEnded(event.getPointerId(actionIndex));
        }
        else{
            touchMoved(event);
        }

        invalidate(); // redraw every thing on the screen
        return true;
    }

    private void touchMoved(MotionEvent event)
    {
        for(int i = 0 ; i < event.getPointerCount(); i++)
        {
            int pointerId = event.getPointerId(i);
            int pointerIndex = event.findPointerIndex(pointerId);

            if(pathMap.containsKey(pointerId))
            {
                float newX = event.getX(pointerIndex);
                float newY = event.getY(pointerIndex);

                Path path = pathMap.get(pointerId);
                Point point = prePointMap.get(pointerId);

                // check how far we went
                float deltaX = Math.abs(newX - point.x);
                float deltaY = Math.abs(newY - point.y);

                //if a movement is big enough to be considered a movement
                if(deltaX >= TOUCH || deltaY >= TOUCH)
                {
                    //move the path to the new point
                    path.quadTo(point.x, point.y, (newX + point.x)/2, (newY + point.y)/2);
                }

                //save the new point
                point.x = (int)newX;
                point.y = (int)newY;
            }
        }
    }

    //the color of the line
    public void setColor(int color)
    {
        paintLine.setColor(color);
    }

    public int getColor()
    {
        return paintLine.getColor();
    }

    //the width of the line
    public void setLineWidth(int Width)
    {
        paintLine.setStrokeWidth(Width);
    }

    public int getLineWidth()
    {
        return (int) paintLine.getStrokeWidth();
    }


    public void clear()
    {
        pathMap.clear();
        prePointMap.clear();
        bitmap.eraseColor(Color.WHITE);
        invalidate(); // redraw every thing on the screen
    }

    private void touchEnded(int pointerId)
    {
        Path path = pathMap.get(pointerId); // get path
        bitmapCanvas.drawPath(path, paintLine); //draw to bitmapCanvas
        path.reset();
    }


    private void touchStarted(float x, float y, int pointerId)
    {
        Path path; // store the path of your touch
        Point point; // store the last point in path

        if(pathMap.containsKey(pointerId)) {
            path = pathMap.get(pointerId);
            point = prePointMap.get(pointerId);
        }
        else
        {
            path = new Path();
            pathMap.put(pointerId, path);

            point = new Point();
            prePointMap.put(pointerId, point);
        }

        // move to the place of touch
        path.moveTo(x, y);
        point.x = (int)x;
        point.y = (int)y;
    }
}
