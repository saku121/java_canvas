package com.myapp.myapplication;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;



public class MainActivity extends AppCompatActivity {
    private AppView appView;
    private AlertDialog.Builder currentAlertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        appView = findViewById(R.id.view);
        //appView = new AppView(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.clearid :
                appView.clear();
                break;

            case R.id.saveId :
                break;

            case R.id.Colorid :
                break;

            case R.id.lineWidth :
                widthDialog();
                break;

            case R.id.Eraser :
                break;
        }
        if(item.getItemId() == R.id.clearid)
        {
            appView.clear();
        }
        return super.onOptionsItemSelected(item);
    }

    void widthDialog()
    {
        currentAlertDialog = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.width_dialog, null);

        SeekBar seekBarWidth = view.findViewById(R.id.widthSeekBar);
        Button buttonWidth = view.findViewById(R.id.widthButton);

        buttonWidth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "new width set", Toast.LENGTH_LONG).show();
            }
        });

        currentAlertDialog.setView(view);
        currentAlertDialog.create();
        currentAlertDialog.show();
    }


}
